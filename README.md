# fmi

[![pipeline status](https://gitlab.com/jondo2010/rust-fmi/badges/master/pipeline.svg)](https://gitlab.com/jondo2010/rust-fmi/commits/master)
[![Build status](https://ci.appveyor.com/api/projects/status/xqvigxmc2tk0bkvm/branch/master?svg=true)](https://ci.appveyor.com/project/jondo2010/rust-fmi/branch/master)
[![codecov](https://codecov.io/gl/jondo2010/rust-fmi/branch/master/graph/badge.svg)](https://codecov.io/gl/jondo2010/rust-fmi)

A Rust interface to FMUs (Functional Mockup Units) that follow the FMI Standard. See http://www.fmi-standard.org/

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
